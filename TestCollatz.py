#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    cycle_length,
)


# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # cycle_length
    # ----

    def test_cycle_length_1(self):
        v = cycle_length(1)
        self.assertEqual(v, 1)

    def test_cycle_length_2(self):
        v = cycle_length(100)
        self.assertEqual(v, 26)

    def test_cycle_length_3(self):
        v = cycle_length(999999)
        self.assertEqual(v, 259)

    def test_cycle_length_4(self):
        v = cycle_length(1000)
        self.assertEqual(v, 112)

    def test_cycle_length_5(self):
        v = cycle_length(999)
        self.assertEqual(v, 50)

    def test_cycle_length_6(self):
        v = cycle_length(1001)
        self.assertEqual(v, 143)

    def test_cycle_length_7(self):
        v = cycle_length(9001)
        self.assertEqual(v, 141)

    def test_cycle_length_8(self):
        v = cycle_length(11)
        self.assertEqual(v, 15)

    def test_cycle_length_9(self):
        v = cycle_length(19)
        self.assertEqual(v, 21)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(999, 1001)
        self.assertEqual(v, 143)

    def test_eval_6(self):
        v = collatz_eval(1999, 2999)
        self.assertEqual(v, 217)

    def test_eval_7(self):
        v = collatz_eval(234, 453)
        self.assertEqual(v, 144)

    def test_eval_8(self):
        v = collatz_eval(1000, 1999)
        self.assertEqual(v, 182)

    def test_eval_9(self):
        v = collatz_eval(1999, 1000)
        self.assertEqual(v, 182)

    def test_eval_10(self):
        v = collatz_eval(100, 1)
        self.assertEqual(v, 119)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
