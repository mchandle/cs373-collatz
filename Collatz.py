#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List, Dict


# ------------
# collatz_read
# ------------
lazyCache: Dict[int, int] = {}


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


def cycle_length(i: int) -> int:
    # check intermediate vals in lazy cache
    result = lazyCache.get(i)
    if not result:
        if i == 1:
            return 1
        if i % 2 == 0:
            nextVal = i >> 1
            result = cycle_length(nextVal)
            lazyCache[nextVal] = result
            return 1 + result
        nextVal = i + (i >> 1) + 1
        result = cycle_length(nextVal)
        lazyCache[nextVal] = result
        # add two because of odd case
        return 2 + result
    return result


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    global lazyCache
    if i > j:
        copy = j
        j = i
        i = copy
    maxCycleLength: int = 0
    # check for optimization
    m = (j // 2) + 1
    if i < m:
        i = m
    for k in range(i, j + 1):
        cycleLength: int = lazyCache.get(k)
        if not cycleLength:
            cycleLength = cycle_length(k)
            lazyCache[k] = cycleLength
        if cycleLength > maxCycleLength:
            maxCycleLength = cycleLength
    return maxCycleLength


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    global lazyCache
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
